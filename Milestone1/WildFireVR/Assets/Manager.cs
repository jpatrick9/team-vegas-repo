﻿/*
name: Alejandro Ruvalcaba
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {
	public AudioClip dig;
	public AudioClip rocks;
	public AudioClip wood;
	public AudioClip fire;
	public AudioClip water;
	public float vol;
	AudioSource audio; 
	public bool start = true;
	public int ro = 0;
	public int wo = 0;
	public int fi = 0;
	public int wa = 0;
	public float times = 1.0f;
	public int levelChosen = 0;
	public bool goodCampsite = false;
	public bool badCampsite = false;
	public bool averageCampsite = false;
    private int score = 1000;
    public GameObject nonCamp;
    public GameObject gCamp;
    public GameObject aCamp;
    public GameObject bCamp;

    // Use this for initialization
    void Start () {
		audio = GetComponent<AudioSource> ();

	}

	// Update is called once per frame
	void Update () {
		if (start == true && Time.time > times) {
			audio.PlayOneShot (dig, vol);
			start = false;
		}
		if (ro == 1) {
			audio.PlayOneShot (rocks, vol);
			ro = ro +1;
		}
		if (wo == 8) {
			audio.PlayOneShot (wood, vol);
			wo = wo + 1;
		}
		if (fi == 4) {
			audio.PlayOneShot (fire, vol);
			fi = fi + 1;
		}
		if (wa == 1) {
			audio.PlayOneShot (water, vol);
			wa = wa + 1;
		}
        if(levelChosen == 1)
        {
            if(goodCampsite)
            {
                nonCamp.SetActive(false);
                aCamp.SetActive(false);
                bCamp.SetActive(false);
            }
            else if(averageCampsite)
            {
                nonCamp.SetActive(false);
                gCamp.SetActive(false);
                bCamp.SetActive(false);
            }
            else if (badCampsite)
            {
                nonCamp.SetActive(false);
                gCamp.SetActive(false);
                aCamp.SetActive(false);
            }
            levelChosen = 2;
        }
	}
}
