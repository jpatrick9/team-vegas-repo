using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogFire : MonoBehaviour {
	public GameObject firePrefab3;
	public GameObject firePrefab2;
	public GameObject firePrefab1;
	private GameObject clone;
	public bool isOnFire = false;
	public int fireLevel;
	public GameObject pit;

	// Use this for initialization
	void Start () {
		fireLevel = 0;

	}

	// Update is called once per frame
	void Update () {
		

	}

	void OnTriggerEnter(Collider collision){
		if (isOnFire && collision.tag == "water")
		{
				Vector3 pos = clone.transform.position;
				Quaternion rot = clone.transform.rotation;
			

			if (fireLevel == 3)
			{
				Destroy(clone, 1.0f);
				clone = (GameObject)Instantiate(firePrefab2, pos, rot);
				clone.transform.parent = transform;
			}
			if (fireLevel == 2)
			{
				Destroy(clone, 1.0f);
				clone = (GameObject)Instantiate(firePrefab1, pos, rot);
				clone.transform.parent = transform;
			}
			if (fireLevel == 1)
			{
				Destroy(clone, 1.0f);
				isOnFire = false;
			}
			fireLevel--;

		}
	}

	void OnCollisionEnter(Collision collision)
	{
		
		if (!isOnFire && collision.collider.tag == "fire")
		{
			
		}
	
	}
}