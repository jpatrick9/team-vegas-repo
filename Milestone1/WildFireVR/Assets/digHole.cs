﻿/*
name: Alejandro Ruvalcaba
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class digHole : MonoBehaviour {
	
	private Vector3 pos;
	private Quaternion rot;
	public GameObject fps;
	public GameObject fpm;
	public GameObject fpl;
	private int hitCount = 0;
	private GameObject lastHole;
	private float timeInterval = 1.5F;
	private float nextHit;
	public bool finish = false;
	public bool inHand = false;
	// Use this for initialization
	public GameObject lvl;
	void Start () {
		rot = Quaternion.Euler(0,90,0);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnCollisionEnter(Collision collision) {

		if(inHand) {
			
			if (!finish ) {
				ContactPoint contact = collision.contacts [0];
				if (collision.collider.tag == "level" && Time.time > nextHit) {
					hitCount = 0;
					if (lastHole != null) {
						Destroy (lastHole);
					}
					pos = contact.point;
					pos.y = -0.11f;
					lastHole = (GameObject)Instantiate (fps, pos, rot);
					lastHole.GetComponent<firePit> ().shovel = this.gameObject;
					hitCount = hitCount + 1;
					nextHit = Time.time + timeInterval;
					lvl.GetComponent<Manager> ().ro = lvl.GetComponent<Manager> ().ro + 1;

				}
				if (collision.collider.tag == "dirt" && Time.time > nextHit) {
					if (hitCount == 1) {

						Destroy (lastHole);
						lastHole = (GameObject)Instantiate (fpm, pos, rot);
						lastHole.GetComponent<firePit> ().shovel = this.gameObject;
						hitCount = hitCount + 1;
				
					} else if (hitCount == 2) {

						Destroy (lastHole);
						lastHole = (GameObject)Instantiate (fpl, pos, rot);
						lastHole.GetComponent<firePit> ().shovel = this.gameObject;
						hitCount = hitCount + 1;
					} else if (hitCount == 3) {

						Destroy (lastHole);
						hitCount = 0;
						lastHole = null;
					}
					nextHit = Time.time + timeInterval;
				}
			}
		}
	}

}
