﻿/*
name: Alejandro Ruvalcaba, Joshua Patrick, Adrian Martinez
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class firePit : MonoBehaviour
{
    public bool fuel = false;
    public bool fire = false;
    public GameObject largeFire;
    public GameObject mediumFire;
    public GameObject smallFire;
    public GameObject smoke;
    private GameObject lastFire;
    private int fireLvl = 0;
    private Vector3 pos;
    private Quaternion rot;
    private float nextDouse;
    private float timeInterval = 1f;
    public GameObject shovel;
    public ParticleSystem water;
    public List<ParticleCollisionEvent> collisionEvents;
    // Use this for initialization
    void Start()
    {
        rot = Quaternion.Euler(270, 0, 0);
        pos = transform.position;
        water = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }
    // Update is called once per frame
    void Update()
    {
        if (fire && fuel && fireLvl != -1)
        {
            fuel = false;
            fireLvl = 3;
            lastFire = (GameObject)Instantiate(largeFire, pos, rot);
            shovel.GetComponent<digHole>().finish = true;
        }
    }
    void OnTriggerEnter(Collider collision)
    {
        if (Time.time > nextDouse)
        {
            if (fire && collision.tag == "water" && fireLvl > 0)
            {
                if (fireLvl == 3)
                {
                    Destroy(lastFire);
                    lastFire = (GameObject)Instantiate(mediumFire, pos, rot);
                    fireLvl = fireLvl - 1;
                }
                else if (fireLvl == 2)
                {
                    Destroy(lastFire);
                    lastFire = (GameObject)Instantiate(smallFire, pos, rot);
                    fireLvl = fireLvl - 1;
                }
                else if (fireLvl == 1)
                {
                    Destroy(lastFire);
                    lastFire = (GameObject)Instantiate(smoke, pos, rot);
                    lastFire = null;
                    fireLvl = -1;
                }

            }
            nextDouse = Time.time + timeInterval;
        }
    }
}
