﻿/*
name: Alejandro Ruvalcaba
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ControllerGrabObject : MonoBehaviour {
	
	private SteamVR_TrackedObject trackedObj;
	private GameObject collidingObject;
	private GameObject objectInHand;
	private bool rhand;
	private bool lhand;
	public GameObject otherHand;

	private SteamVR_Controller.Device Controller{
		get{return SteamVR_Controller.Input((int)trackedObj.index);}
	}

	void Awake(){
		trackedObj = GetComponent<SteamVR_TrackedObject> ();
		rhand = false;
		lhand = false;
		if (tag == "lhand") {
			lhand = true;
		} 
		else {
			rhand = true;
		}
	}

	private void SetCollidingObject(Collider col){
		if (collidingObject || !col.GetComponent<Rigidbody> ()) {
			return;
		}
		collidingObject = col.gameObject;
	}

	public void OnTriggerEnter(Collider other){				//Ingame idea: Highlight the outlines of the object you're about to select?
		SetCollidingObject(other);
	}

	public void OnTriggerStay(Collider other){
		SetCollidingObject(other);
		if (other.tag == "Start")
		{
			if (Controller.GetHairTriggerDown())
			{
				SceneManager.LoadScene ("MainScene");
			}
		}
		if (other.tag == "Quit")
		{
			if (Controller.GetHairTriggerDown())
			{
				//if(UnityEditor
				UnityEditor.EditorApplication.isPlaying=false;
				Application.Quit ();
			}
		}
	}

	public void OnTriggerExit(Collider other){
		if (!collidingObject) {
			return;
		}
		collidingObject = null;
	}

	private void GrabObject(){
		objectInHand = collidingObject;
		collidingObject = null;

		var joint = AddFixedJoint();
		joint.connectedBody = objectInHand.GetComponent<Rigidbody> ();

		if(objectInHand.tag == "shovel") {
			objectInHand.GetComponent<digHole> ().inHand = true;
		}
		if(objectInHand.tag == "backpack") {
			objectInHand.GetComponent<levelSelect> ().canTeleport = true;
		}




	}

	private FixedJoint AddFixedJoint(){
		FixedJoint fx = gameObject.AddComponent<FixedJoint> ();
		fx.breakForce = 20000;
		fx.breakTorque = 20000;
		return fx;
	}

	private void ReleaseObject(){

		if (GetComponent<FixedJoint>()) {

			GetComponent<FixedJoint>().connectedBody = null;
			Destroy (GetComponent<FixedJoint>());

			objectInHand.GetComponent<Rigidbody> ().velocity = Controller.velocity;
			objectInHand.GetComponent<Rigidbody> ().angularVelocity = Controller.angularVelocity;
		}

		if(objectInHand.tag == "shovel") {
			if(otherHand.GetComponent<ControllerGrabObject>().objectInHand == null) 
			{
				objectInHand.GetComponent<digHole> ().inHand = false;
			}
			else if(otherHand.GetComponent<ControllerGrabObject>().objectInHand.tag != "shovel")
			{
				objectInHand.GetComponent<digHole> ().inHand = false;
			}
			else
			{
				objectInHand.GetComponent<digHole> ().inHand = true;
			}
		}
		if(objectInHand.tag == "backpack") {
			if(otherHand.GetComponent<ControllerGrabObject>().objectInHand == null) 
			{
				objectInHand.GetComponent<levelSelect> ().canTeleport = false;
			}
			else if(otherHand.GetComponent<ControllerGrabObject>().objectInHand.tag != "backpack")
			{
				objectInHand.GetComponent<levelSelect> ().canTeleport = false;
			}
			else
			{
				objectInHand.GetComponent<levelSelect> ().canTeleport = true;
			}
		}

		objectInHand = null;


	}


	// Update is called once per frame
	void Update () {

		if (Controller.GetHairTriggerDown ()) {
			if (collidingObject) {
				GrabObject ();
			}
		}

		if (Controller.GetHairTriggerUp ()) {
			if (objectInHand) {
				ReleaseObject ();
			}
		}
	}
}
