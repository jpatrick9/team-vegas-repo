﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    public void LoadMyScene(int level)
    {
        SceneManager.LoadScene(level);
    }

    public void QuitScene()
    {
        Application.Quit();
        Debug.Log("Game is quitting!");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
