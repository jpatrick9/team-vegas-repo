﻿/*
name: Adrian Martinez
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class percent : MonoBehaviour 
{
	public double firep; // whats the percentage your fire would have gotten out of hand
	public double timep; // how long your fire would have lasted
	public bool litp; // did player able to light a fire or not
	public bool putoutf;
	public int total;
	// Use this for initialization
	void Start () 
	{
		//this script will later be connected to another script to pass on the values
		firep = 0; 
		timep = 0;
		litp = false;
		total = 100;
		putoutf = false;	
	}
	// Update is called once per frame
	void Update () 
	{
		if (total == 100 && litp == true)
		{
			Debug.Log ("You have made a perfect fire");
		}
		if (total <= 99  && total >= 90 &&litp == true) 
		{
			Debug.Log ("You have made a amazing fire");
		}
		if (total <= 89  && total >= 80 &&litp == true)
		{
			Debug.Log ("You have made a great fire");
		}
		if (total <= 79  && total >= 70 &&litp == true) 
		{
			Debug.Log ("You have made a great fire");
		}
		if (total <= 69  && total >= 60 &&litp == true) 
		{
			Debug.Log ("You have made an okay fire");
		}
		if (total <= 59  && total >= 50 &&litp == true) 
		{
			Debug.Log ("You have made a risky fire");
		}
		if (total <= 49  && total >= 0 &&litp == true) 
		{
			Debug.Log ("You have made a dangerous fire");
		}
		if (litp == false) 
		{
			Debug.Log ("You were not able to start a fire");
		}
		if(putoutf == false)
		{
			Debug.Log ("YYou did not put out your fire");
		}

	}
}
