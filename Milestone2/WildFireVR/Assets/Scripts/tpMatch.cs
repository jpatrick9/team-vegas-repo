﻿/*
name: Adrian Martinez
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tpMatch : MonoBehaviour 
{
	public float floor;
	public Vector3 pos;
	// Use this for initialization
	void Start () 
	{
		pos= transform.position;
		Debug.Log(" Match pos is " + pos);
	}
	// Update is called once per frame
	void Update () 
	{
		if (transform.position.y < floor)
		{
			transform.position = new Vector3(-1.128f, 0.831f, -0.233f);
		}
	}
}
