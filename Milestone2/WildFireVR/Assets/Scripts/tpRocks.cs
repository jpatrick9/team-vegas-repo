﻿/*
name: Adrian Martinez
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tpRocks : MonoBehaviour 
{
	public float floor;
	public Vector3 pos;

	// Use this for initialization
	void Start () 
	{
		pos= transform.position;
		Debug.Log("Rock pos is " + pos);
	}
	// Update is called once per frame
	void Update () 
	{
		if (transform.position.y < floor) 
		{
			transform.position = new Vector3 (-0.792f, 0.874f, 0.263f);
		}
	}
}