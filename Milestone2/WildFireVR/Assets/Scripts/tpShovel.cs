﻿/*
name: Adrian Martinez
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tpShovel : MonoBehaviour
{
	public float floor;
	public Vector3 pos;
	// Use this for initialization
	void Start () 
	{
		pos= transform.position;
		Debug.Log("shovel pos is " + pos);
	}
	// Update is called once per frame
	void Update ()
	{
		if (transform.position.y < floor)
		{
			transform.position = new Vector3(-0.804f, 0.824f, -0.015f);
		}
	}
}
