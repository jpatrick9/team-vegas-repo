﻿/*
name: Brieg Oudeacoumar
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Milestone 2 UPDATE :
 * - added Log Counter
 * - added Red/Grey Ash mechanic
 * - added FireTimer
 * - fixed Particle systems
 * - fixed Fire Stages Transitions
 * - code clarification
*/

public class firePit : MonoBehaviour {

	public bool alreadyUsed = false;
	public bool fuel = false;
	public bool fire = false;
	public bool isRedAsh = false;
	public float fireTimer = 0;
	public int nbOfLogs = 0;
	public GameObject largeFire;
	public GameObject mediumFire;
	public GameObject smallFire;
	public GameObject smoke;
	public GameObject ashBlack;
	public GameObject ashRed;
	private GameObject lastFire;
	private GameObject lastAsh;
	private int fireLvl = 0;
	private Vector3 pos;
	private Vector3 pos2;
	private Quaternion rot;
	private Quaternion rot2;
	private float nextDouse;
	private float timeInterval = 1f;
	public GameObject shovel;
	public ParticleSystem water;
	public GameObject lvlManager;
	public List<ParticleCollisionEvent> collisionEvents;
	// Use this for initialization
	void Start()
	{
		//Initialization of variables
		rot = Quaternion.Euler(270, 0, 0);
		rot2 = Quaternion.Euler(0, 0, 0);
		pos = transform.position;
		pos2 = new Vector3(transform.position.x, transform.position.y + 1, transform.position.z);
		water = GetComponent<ParticleSystem>();
		collisionEvents = new List<ParticleCollisionEvent>();
		//All logs

	}
	// Update is called once per frame
	void Update()
	{
		//Reduce fireTimer by 1 second every second
		fireTimer -= Time.deltaTime;
		//If FirePit on Fire and fireTimer reaches 0
		if (fire && fireTimer < 0)
		{
			Debug.Log("fireTimer reached 0");
			fireLvl = 0;
			fire = false;
			alreadyUsed = true;
			Destroy (lastFire);
			Debug.Log("Turning Logs to RedAsh");
			isRedAsh = true;
			//Instantiate Smoke Particles
			lastFire = (GameObject)Instantiate(smoke, pos, rot);
			//Instantiate RedAsh Prefab
			lastAsh = (GameObject)Instantiate(ashRed, pos2, rot2);
			Debug.Log("FirePit is now RedAsh");
		}
		//If FirePit is on Fire and Logs attached to FirePit and Firelvl > 0
		if (fire && fuel && fireLvl != -1)
		{
			//Consume Logs
			fuel = false; 
			//Set fireLvl to maximum 
			fireLvl = 3;
			lastFire = (GameObject)Instantiate(largeFire, pos, rot);
			//Disable FirePit digging
			shovel.GetComponent<digHole>().finish = true;
		}
	}


	//Extinguish fire
	void OnTriggerEnter(Collider collision)
	{
		Debug.Log ("Triggered with" + collision.tag);
		if (Time.time > nextDouse)
		{
			//If FirePit is on Fire and meets water and is RedAsh
			if (fire && collision.tag == "water" && isRedAsh)
			{
				Debug.Log("RedAsh met Water");
				Debug.Log("Turning RedAsh to BlackAsh");
				//Destroy RedAsh
				Destroy(lastAsh);
				//Destroy Smoke
				Destroy(lastFire);
				//Instantiate BlackAsh
				lastAsh = (GameObject)Instantiate(ashBlack, pos2, rot2);
				Debug.Log("FirePit off");
				//Update LevelManager
				lvlManager.GetComponent<Manager>().calculateScore = true;
			}
			//If FirePit is on Fire and meets water and fireLvl greater than 0
			if (fire && collision.tag == "water")
			{
				Debug.Log("FirePit met water");
				//If FirePit level to max (3)
				if (fireLvl == 3)
				{
					//Destroy previous fire
					Destroy(lastFire);
					//Instantiate smaller fire
					lastFire = (GameObject)Instantiate(mediumFire, pos, rot);
					fireLvl = fireLvl - 1;
					Debug.Log("fireLvl set to : " + fireLvl);
				}
				else if (fireLvl == 2)
				{
					//Destroy previous fire
					Destroy(lastFire);
					//Instantiate smaller fire
					lastFire = (GameObject)Instantiate(smallFire, pos, rot);
					fireLvl = fireLvl - 1;
					Debug.Log("fireLvl set to : " + fireLvl);
				}
				else if (fireLvl == 1)
				{
					Debug.Log("Turning FirePit to BlackAsh");
					//Destroy previous fire
					Destroy(lastFire);
					alreadyUsed = true;
					//Instantiate smaller fire
					lastFire = (GameObject)Instantiate(smoke, pos, rot);
					//Instantiate RedAsh
					lastAsh = (GameObject)Instantiate(ashBlack, pos2, rot2);
					lastFire = null;
					fireLvl = -1;
					Debug.Log("fireLvl set to : " + fireLvl);
				}

			}
			nextDouse = Time.time + timeInterval;
		}
	}
}