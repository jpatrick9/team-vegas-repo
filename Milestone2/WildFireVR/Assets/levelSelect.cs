﻿/*
name: Alejandro Ruvalcaba
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelSelect : MonoBehaviour {
	public GameObject lvlManager;
    public GameObject lHand;
    public GameObject rHand;
    public bool canTeleport = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(lvlManager.GetComponent<Manager>().levelChosen == 0)
        {
            if(!canTeleport)
            {
                lHand.GetComponent<LaserPointer>().enabled = false;
                rHand.GetComponent<LaserPointer>().enabled = false;
            }
            if(canTeleport)
            {
                lHand.GetComponent<LaserPointer>().enabled = true;
                rHand.GetComponent<LaserPointer>().enabled = true;
            }
        }
        else
        {
            lHand.GetComponent<LaserPointer>().enabled = true;
            rHand.GetComponent<LaserPointer>().enabled = true;
        }
	}
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "BadCampsite")
        {
            lvlManager.GetComponent<Manager>().levelChosen = 1;
            lvlManager.GetComponent<Manager>().badCampsite = true;
        }
        if (collision.collider.tag == "GoodCampsite")
        {
            lvlManager.GetComponent<Manager>().levelChosen = 1;
            lvlManager.GetComponent<Manager>().goodCampsite = true;
        }
        if (collision.collider.tag == "AverageCampsite")
        {
            lvlManager.GetComponent<Manager>().levelChosen = 1;
            lvlManager.GetComponent<Manager>().averageCampsite = true;
        }
    }

}
