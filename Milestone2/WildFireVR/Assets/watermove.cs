﻿/*
name: Adrian Martinez, Joshua Patrick
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class watermove : MonoBehaviour {
	public ParticleSystem water;
	//public GameObject bucket;
	public Quaternion rotation = Quaternion.identity;
    public GameObject bucket;
    private bool isWorking;
    public bool pour = false;
    private int count = 0;
	// Use this for initialization
	void Start () {
		water.Pause ();
        //bucket = GetComponent<Transform>();
		rotation.eulerAngles = new Vector3 (0, 0, 0);
        isWorking = false;
        count = 3;
	}
	// Update is called once per frame
	void Update () {
        if (isWorking)
        {
            if (Mathf.Abs(transform.rotation.eulerAngles.z) > 90f || Mathf.Abs(transform.rotation.eulerAngles.x) > 90f || Mathf.Abs(transform.rotation.eulerAngles.x) > -90f || Mathf.Abs(transform.rotation.eulerAngles.z) > -90f)
            {
                water.Play();
            }
            else
            {
                water.Clear();
                water.Pause();
            }
        }
        if (pour)
        {
            water.Play();
        }
        else
        {
            water.Pause();
            water.Clear();
     
        }

	}
    private void OnTriggerEnter(Collider other)
    {
        pour = true;
    
       
    }
    private void OnTriggerExit(Collider other)
    {
        count = count - 1;
        if (count == 0)
        {
            pour = false;
            count = 3;
        }
            
    }

}