﻿/*
name: Adrian Martinez
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class highlight : MonoBehaviour
{
	public GUISkin GameSkin;
	public string objectName;
	private Color startColour;
	private bool _displayObjectName = false;
	private SteamVR_TrackedObject trackedobj;
	private SteamVR_Controller.Device Controller
	{
		get{return SteamVR_Controller.Input((int)trackedobj.index);}
	}
	void Awake()
	{
		trackedobj = GetComponent<SteamVR_TrackedObject> ();
	}
	void Start () 
	{
	}
	void Update () 
	{
	}
	void onGUI()
	{
		GUI.skin = GameSkin;
		DisplayName ();
	}
	void OnMouseEnter()
	{
		startColour = GetComponent<Renderer> ().material.color;
		GetComponent<Renderer> ().material.color = Color.green;
		_displayObjectName = true;
	}
	void OnMouseExit()
	{
		GetComponent<Renderer> ().material.color = startColour;
		_displayObjectName = false;
	}
	public void DisplayName()
	{
		if (_displayObjectName == true) 
		{
			GUI.Box (new Rect (Event.current.mousePosition.x - 155, Event.current.mousePosition.y, 150, 25), objectName, "Box2 Style");
		}
	}
	void OnTriggerEnter(Collider collision) 
	{
		if (collision.tag == "lhand" || collision.tag == "rhand") 
		{
			if (Controller.GetHairTriggerDown ()) {
				startColour = GetComponent<Renderer> ().material.color;
				GetComponent<Renderer> ().material.color = Color.green;
				_displayObjectName = true;
			} 
			else 
			{
				GetComponent<Renderer> ().material.color = startColour;
				_displayObjectName = false;
			}
		}
}
}