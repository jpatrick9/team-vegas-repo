﻿/*
name: Adrian Martinez
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class settingv : MonoBehaviour 
{
	public Slider Volume;
	public AudioSource music;
	// Use this for initialization
	void Start () 
	{
		
	}
	// Update is called once per frame
	void Update ()
	{
		music.volume = Volume.value;
	}
}
