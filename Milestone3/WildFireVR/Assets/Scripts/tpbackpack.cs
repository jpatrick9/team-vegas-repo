﻿/*
name: Alejandro Ruvalcaba
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class tpbackpack : MonoBehaviour
{
	public GameObject lvl;
	public GameObject player;
	public float floor;
	public Vector3 pos;
	private bool lvlSelected;
	private bool outsidePlayArea;
	void Start () 
	{
		if (lvl.GetComponent<Manager> ().levelChosen == 0) {
			lvlSelected = false;
		} 
		else 
		{
			lvlSelected = true;
		}
		pos= transform.position;
		outsidePlayArea = false;
	}
	void Update () 
	{
	}
	void OnCollisionEnter(Collision collision)
	{
		if(!lvlSelected) 
		{
			if (collision.collider.tag == "outofbound") 
			{
				transform.position = pos;
			}
			else if (collision.collider.tag == "NonCampsite")
			{
				if (outsidePlayArea) 
				{
					transform.position = player.transform.position;
				} 
				else 
				{
					pos= transform.position;
				}

			}
			else if (collision.collider.tag == "level")
			{
				if (outsidePlayArea) 
				{
					transform.position = player.transform.position;
				} 
				else 
				{
					pos= transform.position;
				}
			}
		}
		else{
			if (collision.collider.tag == "outofbound") 
			{
					transform.position = pos;
			} 
			else if (collision.collider.tag == "NonCampsite")
			{
				pos= transform.position;
			}
			else if (collision.collider.tag == "level")
			{
				pos= transform.position;
			}
		}
	}
	void OnTriggerExit(Collider collision)
	{
		if (outsidePlayArea) 
		{
			if (collision.tag == "Player") 
			{
				transform.position = pos;
	
			}
		}
	}
}
