﻿/*
name: Adrian Martinez
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tpbucket1 : MonoBehaviour
{
	public float floor;
	public Vector3 pos;
	public bool onBackpack = false;
	void Start () 
	{
		pos= transform.position;
		Debug.Log("bucket pos is " + pos);
	}
	void Update () 
	{
	}
	void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.tag == "outofbound") 
		{
			transform.position = pos;
		} 
		else if (collision.collider.tag == "NonCampsite")
		{
			pos= transform.position;
		}
		else if (collision.collider.tag == "level")
		{
			pos= transform.position;
		}
	}
}
