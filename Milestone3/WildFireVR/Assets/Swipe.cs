﻿/*
name: Alejandro Ruvalcaba
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swipe : MonoBehaviour {

	public bool swiped;
	public GameObject left;
	public GameObject middle;
	public GameObject right;
	public bool leftSwipe;
	public bool rightSwipe;
	public bool nonSwiped;
	public bool reset;
	public bool lhand;
	public bool rhand;
	// Use this for initialization
	void Start () {
		leftSwipe = false;
		rightSwipe = false;
		nonSwiped = true;
		reset = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (reset) 
		{
			leftSwipe = false;
			rightSwipe = false;
			nonSwiped = true;
			reset = false;
		}
	}
	void OnTriggerEnter(Collider collision)
	{
		if (collision.tag == "lhand" && !lhand) 
		{
			if (tag == "left") 
			{
				if (middle.GetComponent<Swipe> ().nonSwiped && right.GetComponent<Swipe>().nonSwiped) {
					leftSwipe = true;
					nonSwiped = false;
				}
				else if(middle.GetComponent<Swipe>().leftSwipe && right.GetComponent<Swipe>().leftSwipe) 
				{
					rightSwipe= true;
					nonSwiped = false;
					Debug.Log ("Page Swiped to the right");
				}
				else 
				{
					reset = true;
					middle.GetComponent<Swipe> ().reset = true;
					right.GetComponent<Swipe> ().reset = true;
				}

			} else if (tag == "middle") 
			{
				if(left.GetComponent<Swipe>().leftSwipe && right.GetComponent<Swipe>().nonSwiped)
				{
					leftSwipe = true;
					nonSwiped = false;
				}
				else if(right.GetComponent<Swipe>().rightSwipe && left.GetComponent<Swipe>().nonSwiped)
				{
					rightSwipe = true;
					nonSwiped = false;
				}
				else
				{
					reset = true;
					left.GetComponent<Swipe> ().reset = true;
					right.GetComponent<Swipe> ().reset = true;
				}
			} else
			{
				if (middle.GetComponent<Swipe> ().nonSwiped && left.GetComponent<Swipe>().nonSwiped) {
					rightSwipe = true;
					nonSwiped = false;
				}
				else if(middle.GetComponent<Swipe>().rightSwipe && left.GetComponent<Swipe>().rightSwipe) 
				{
					leftSwipe = true;
					nonSwiped = false;
					Debug.Log ("Page Swiped to the left");
				}
				else 
				{
					reset = true;
					middle.GetComponent<Swipe> ().reset = true;
					left.GetComponent<Swipe> ().reset = true;
				}

			}
		} else if (collision.tag == "rhand" && !rhand) 
		{
			if (tag == "left") 
			{
				if (middle.GetComponent<Swipe> ().nonSwiped && right.GetComponent<Swipe>().nonSwiped) {
					leftSwipe = true;
					nonSwiped = false;
				}
				else if(middle.GetComponent<Swipe>().leftSwipe && right.GetComponent<Swipe>().leftSwipe) 
				{
					rightSwipe= true;
					nonSwiped = false;
					Debug.Log ("Page Swiped to the right");
				}
				else 
				{
					reset = true;
					middle.GetComponent<Swipe> ().reset = true;
					right.GetComponent<Swipe> ().reset = true;
				}

			} else if (tag == "middle") 
			{
				if(left.GetComponent<Swipe>().leftSwipe && right.GetComponent<Swipe>().nonSwiped)
				{
					leftSwipe = true;
					nonSwiped = false;
				}
				else if(right.GetComponent<Swipe>().rightSwipe && left.GetComponent<Swipe>().nonSwiped)
				{
					rightSwipe = true;
					nonSwiped = false;
				}
				else
				{
					reset = true;
					left.GetComponent<Swipe> ().reset = true;
					right.GetComponent<Swipe> ().reset = true;
				}
			} else
			{
				if (middle.GetComponent<Swipe> ().nonSwiped && left.GetComponent<Swipe>().nonSwiped) {
					rightSwipe = true;
					nonSwiped = false;
				}
				else if(middle.GetComponent<Swipe>().rightSwipe && left.GetComponent<Swipe>().rightSwipe) 
				{
					leftSwipe = true;
					nonSwiped = false;
					Debug.Log ("Page Swiped to the left");
				}
				else 
				{
					reset = true;
					middle.GetComponent<Swipe> ().reset = true;
					left.GetComponent<Swipe> ().reset = true;
				}

			}
		} else 
		{
			//Do Nothing
		}
	}
}