﻿/*
name: Alejandro Ruvalcaba
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tabletNavigation : MonoBehaviour {

	public GameObject left;
	public GameObject middle;
	public GameObject right;
	public bool leftHand = false;
	public bool rightHand = false;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (leftHand) {
			left.GetComponent<Swipe> ().lhand = true;
			middle.GetComponent<Swipe> ().lhand = true;
			right.GetComponent<Swipe> ().lhand = true;
		}
		if (rightHand) {
			left.GetComponent<Swipe> ().rhand = true;
			middle.GetComponent<Swipe> ().rhand = true;
			right.GetComponent<Swipe> ().rhand = true;
		}
		if(!leftHand && !rightHand)
		{
			left.GetComponent<Swipe> ().lhand = false;
			middle.GetComponent<Swipe> ().lhand = false;
			right.GetComponent<Swipe> ().lhand = false;
			left.GetComponent<Swipe> ().rhand = false;
			middle.GetComponent<Swipe> ().rhand = false;
			right.GetComponent<Swipe> ().rhand = false;
		}
		if(left.GetComponent<Swipe>().leftSwipe && middle.GetComponent<Swipe>().leftSwipe && right.GetComponent<Swipe>().leftSwipe)
		{
			Debug.Log ("Left Swipe");
			left.GetComponent<Swipe> ().reset = true;
			middle.GetComponent<Swipe> ().reset = true;
			right.GetComponent<Swipe> ().reset = true;
		}
		else if(left.GetComponent<Swipe>().rightSwipe && middle.GetComponent<Swipe>().rightSwipe && right.GetComponent<Swipe>().rightSwipe)
		{
			Debug.Log ("Right Swipe");
			left.GetComponent<Swipe> ().reset = true;
			middle.GetComponent<Swipe> ().reset = true;
			right.GetComponent<Swipe> ().reset = true;
		}
		else {
			//Do Nothing
		}
	}
}
