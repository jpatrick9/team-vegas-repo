﻿/*
name: Alejandro Ruvalcaba
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {
	public GameObject tablet;
	public AudioClip a1;
	public AudioClip a2;
	public AudioClip a3;
	public AudioClip a4;
	public AudioClip a5;
	public AudioClip a6;
	public AudioClip a7;
	public AudioClip a8;
	public AudioClip a9;
	public AudioClip a10;
	public AudioClip a11;
	public AudioClip a12;
	public AudioClip a13;
	public AudioClip a14;
	public AudioClip a15;
	public AudioClip a16;
	public AudioClip a17;
	public AudioClip a18;
	public AudioClip a19;
	public AudioClip a20;
	public AudioClip a21;
	public AudioClip a22;
	public AudioClip a23;
	public AudioClip a24;
	public AudioClip a25;
	public AudioClip a26;
	public AudioClip a27;
	public AudioClip a28;
	public AudioClip a29;
	public AudioClip a30;
	public AudioClip a31;
	public float vol;
	AudioSource audio; 
	public bool start = true;
	public int ro = 0;
	public int wo = 0;
	public int fi = 0;
	public int wa = 0;
	public float times = 1.0f;
	public int levelChosen = 0;
	public bool goodCampsite = false;
	public bool badCampsite = false;
	public bool averageCampsite = false;
    private int score = 10000;
    public GameObject nonCamp;
    public GameObject gCamp;
	public GameObject gRocksLogs;
    public GameObject aCamp;
	public GameObject aRocksLogs;
    public GameObject bCamp;
	public GameObject bRocksLogs;
	public bool smallHole;
	public bool mediumHole;
	public bool largeHole;
	public bool calculateScore;
	public GameObject[] validRocks;
	public GameObject[] validLogs;
	private int logDeduction = 0;
	private int rockDeduction = 0;
	private int campsiteDeduction = 0;
	private int holeDeduction = 0;
	private int numOfLogs = 0;
	private int numOfGoodRocks = 0;
	private int numOfBadRocks = 0;
	private int totalRocks = 0;
	private bool finishedCalculating = false;
	public int pain = 0;

    // Use this for initialization
    void Start () {
		audio = GetComponent<AudioSource> ();
		smallHole = false;
		mediumHole = false;
		largeHole = false;
		calculateScore = false;
		gRocksLogs.SetActive (false);
		aRocksLogs.SetActive (false);
		bRocksLogs.SetActive (false);
	}

	// Update is called once per frame


	void Update () {
		if(calculateScore && !finishedCalculating) {
			
			if(goodCampsite) {
				if (validLogs == null) 
				{
					
					validLogs = GameObject.FindGameObjectsWithTag ("log");
				}
				if (validRocks == null) 
				{

					validRocks = GameObject.FindGameObjectsWithTag ("Rocks");
				}

				foreach (GameObject log in validLogs)
				{
					if (log.GetComponent<Placement> ().isFrozen) 
					{
						numOfLogs = numOfLogs + 1;
					}
						
				}
				foreach (GameObject rock in validRocks)
				{
					if(rock.GetComponent<rockCounter>().inDirt)
					{
						numOfBadRocks = numOfBadRocks + 1;
					}
					if(rock.GetComponent<rockCounter>().inRock) 
					{
						numOfGoodRocks = numOfGoodRocks + 1;
					}
				}
				totalRocks = numOfGoodRocks + numOfBadRocks;
				//hole deduction and score computation
				if(smallHole) 
				{
					//logs should be 2-3
					holeDeduction = 100;
					if(numOfLogs > 3) 
					{
						logDeduction = (numOfLogs - 3) * 75;
					}
					else if (numOfLogs <2) 
					{
						logDeduction = (2 - numOfLogs) * 25;
					}
					if(totalRocks >8) 
					{
						rockDeduction = (numOfBadRocks * 50);
						rockDeduction = rockDeduction + ((totalRocks - 8) * 25);
					}
					else if(totalRocks < 6)
					{
						rockDeduction = (numOfBadRocks * 50);
						rockDeduction = rockDeduction + ((6 - totalRocks) * 100);
					}
					else
					{
						rockDeduction = (numOfBadRocks * 50);
					}

				}
				else if(mediumHole) 
				{
					//logs should be 3-4
					if(numOfLogs > 4) 
					{
						logDeduction = (numOfLogs - 4) * 75;
					}
					else if (numOfLogs <3) 
					{
						logDeduction = (3 - numOfLogs) * 25;
					}
					if(totalRocks >12)
					{
						rockDeduction = (numOfBadRocks * 50);
						rockDeduction = rockDeduction + ((totalRocks - 12) * 25);
					}
					else if(totalRocks < 10)
					{
					
						rockDeduction = (numOfBadRocks * 50);
						rockDeduction = rockDeduction + ((10 - totalRocks) * 100);
					}
					else
					{
						rockDeduction = (numOfBadRocks * 50);
					}

				}
				else if(largeHole) 
				{
					holeDeduction = 300;
					//logs should be 3-5
					if(numOfLogs > 5)
					{
						logDeduction = (numOfLogs - 5) * 75;
					}
					else if (numOfLogs <3) 
					{
						logDeduction = (3 - numOfLogs) * 25;
					}
					if(totalRocks >16)
					{
					rockDeduction = (numOfBadRocks * 50);
					rockDeduction = rockDeduction + ((totalRocks - 16) * 25);
					}

					else if(totalRocks < 14) 
					{
						rockDeduction = (numOfBadRocks * 50);
						rockDeduction = rockDeduction + ((14 - totalRocks) * 100);
					}
					else
					{
						rockDeduction = (numOfBadRocks * 50);
					}

				}


			}

			else if(averageCampsite) {
				if (validLogs == null) 
				{
					
					validLogs = GameObject.FindGameObjectsWithTag ("log");
				}
				if (validRocks == null)
				{

					validRocks = GameObject.FindGameObjectsWithTag ("Rocks");
				}
				foreach (GameObject log in validLogs)
				{
					if (log.GetComponent<Placement> ().isFrozen)
					{
						numOfLogs = numOfLogs + 1;
					}
				}
				foreach (GameObject rock in validRocks)
				{
					if(rock.GetComponent<rockCounter>().inDirt) 
					{
						numOfBadRocks = numOfBadRocks + 1;
					}
					if(rock.GetComponent<rockCounter>().inRock) 
					{
						numOfGoodRocks = numOfGoodRocks + 1;
					}
				}
				//campsite deduction
				campsiteDeduction = 200;
				totalRocks = numOfGoodRocks + numOfBadRocks;
				//hole deduction and score computation
				if(smallHole)
				{
					//logs should be 2-3
					holeDeduction = 100;
					if(numOfLogs > 3)
					{
						logDeduction = (numOfLogs - 3) * 75;
					}
					else if (numOfLogs <2) 
					{
						logDeduction = (2 - numOfLogs) * 25;
					}
					if(totalRocks >8) 
					{
						rockDeduction = (numOfBadRocks * 50);
						rockDeduction = rockDeduction + ((totalRocks - 8) * 25);
					}
					else if(totalRocks < 6) 
					{
						rockDeduction = (numOfBadRocks * 50);
						rockDeduction = rockDeduction + ((6 - totalRocks) * 100);
					}
					else
					{
						rockDeduction = (numOfBadRocks * 50);
					}

				}
				else if(mediumHole) {
					//logs should be 3-4
					if(numOfLogs > 4) 
					{
						logDeduction = (numOfLogs - 4) * 75;
					}
					else if (numOfLogs <3)
					{
						logDeduction = (3 - numOfLogs) * 25;
					}
					if(totalRocks >12) 
					{
						rockDeduction = (numOfBadRocks * 50);
						rockDeduction = rockDeduction + ((totalRocks - 12) * 25);
					}
					else if(totalRocks < 10)
					{

						rockDeduction = (numOfBadRocks * 50);
						rockDeduction = rockDeduction + ((10 - totalRocks) * 100);
					}
					else
					{
						rockDeduction = (numOfBadRocks * 50);
					}

				}
				else if(largeHole) {
					holeDeduction = 300;
					//logs should be 3-5
					if(numOfLogs > 5) 
					{
						logDeduction = (numOfLogs - 5) * 75;
					}
					else if (numOfLogs <3) 
					{
						logDeduction = (3 - numOfLogs) * 25;
					}
					if(totalRocks >16) 
					{
						rockDeduction = (numOfBadRocks * 50);
						rockDeduction = rockDeduction + ((totalRocks - 16) * 25);
					}

					else if(totalRocks < 14) 
					{
						rockDeduction = (numOfBadRocks * 50);
						rockDeduction = rockDeduction + ((14 - totalRocks) * 100);
					}
					else
					{
						rockDeduction = (numOfBadRocks * 50);
					}

				}



			}

			else if(badCampsite)
			{
				if (validLogs == null)
				{
					
					validLogs = GameObject.FindGameObjectsWithTag ("log");
				}
				if (validRocks == null) 
				{

					validRocks = GameObject.FindGameObjectsWithTag ("Rocks");
				}
				foreach (GameObject log in validLogs)
				{
					if (log.GetComponent<Placement> ().isFrozen)
					{
						numOfLogs = numOfLogs + 1;
					}
				}
				foreach (GameObject rock in validRocks)
				{
					if(rock.GetComponent<rockCounter>().inDirt)
					{
						numOfBadRocks = numOfBadRocks + 1;
					}
					if(rock.GetComponent<rockCounter>().inRock) 
					{
						numOfGoodRocks = numOfGoodRocks + 1;
					}
				}
				//campsite deduction
				campsiteDeduction = 400;
				totalRocks = numOfGoodRocks + numOfBadRocks;
				//hole deduction and score computation
				if(smallHole) 
				{
					//logs should be 2-3
					holeDeduction = 100;
					if(numOfLogs > 3)
					{
						logDeduction = (numOfLogs - 3) * 75;
					}
					else if (numOfLogs <2)
					{
						logDeduction = (2 - numOfLogs) * 25;
					}
					if(totalRocks >8) 
					{
						rockDeduction = (numOfBadRocks * 50);
						rockDeduction = rockDeduction + ((totalRocks - 8) * 25);
					}
					else if(totalRocks < 6) 
					{
						rockDeduction = (numOfBadRocks * 50);
						rockDeduction = rockDeduction + ((6 - totalRocks) * 100);
					}
					else
					{
						rockDeduction = (numOfBadRocks * 50);
					}

				}
				else if(mediumHole)
				{
					//logs should be 3-4
					if(numOfLogs > 4) 
					{
						logDeduction = (numOfLogs - 4) * 75;
					}
					else if (numOfLogs <3)
					{
						logDeduction = (3 - numOfLogs) * 25;
					}
					if(totalRocks >12)
					{
						rockDeduction = (numOfBadRocks * 50);
						rockDeduction = rockDeduction + ((totalRocks - 12) * 25);
					}
					else if(totalRocks < 10) 
					{

						rockDeduction = (numOfBadRocks * 50);
						rockDeduction = rockDeduction + ((10 - totalRocks) * 100);
					}
					else
					{
						rockDeduction = (numOfBadRocks * 50);
					}

				}
				else if(largeHole) 
				{
					holeDeduction = 300;
					//logs should be 3-5
					if(numOfLogs > 5) 
					{
						logDeduction = (numOfLogs - 5) * 75;
					}
					else if (numOfLogs <3)
					{
						logDeduction = (3 - numOfLogs) * 25;
					}
					if(totalRocks >16) {
						rockDeduction = (numOfBadRocks * 50);
						rockDeduction = rockDeduction + ((totalRocks - 16) * 25);
					}

					else if(totalRocks < 14) 
					{
						rockDeduction = (numOfBadRocks * 50);
						rockDeduction = rockDeduction + ((14 - totalRocks) * 100);
					}
					else
					{
						rockDeduction = (numOfBadRocks * 50);
					}

				}


			}
			score = score - holeDeduction;
			score = score - logDeduction;
			score = score - rockDeduction;
			score = score - campsiteDeduction;
			Debug.Log ("Your score out of 10000 was: " + score);
			finishedCalculating = true;
				
		}
		if(levelChosen == 1){
			//grab a shovel
			start = false;

		}
		if (start == true && Time.time > times) 
		{
			
		}
		if (ro == 1) 
		{
			//i should surround with rocks
			ro = ro +1;
		}
		if (wo == 8) 
		{
			//i should put wood in
			wo = wo + 1;
		}
		if (fi == 4) 
		{
			//match time
			fi = fi + 1;
		}
		if (wa == 1)
		{
			//put fire out
			wa = wa + 1;
		}
        if(levelChosen == 1)
        {
            if(goodCampsite)
            {
				gRocksLogs.SetActive (true);
                nonCamp.SetActive(false);
                aCamp.SetActive(false);
                bCamp.SetActive(false);
				//tablet.transform.position = new Vector3 ();

            }
            else if(averageCampsite)
            {
				aRocksLogs.SetActive (true);
                nonCamp.SetActive(false);
                gCamp.SetActive(false);
                bCamp.SetActive(false);
				//tablet.transform.position = new Vector3 ();
            }
            else if (badCampsite)
            {
				bRocksLogs.SetActive (true);
                nonCamp.SetActive(false);
                gCamp.SetActive(false);
                aCamp.SetActive(false);
				//tablet.transform.position = new Vector3 ();
            }
            levelChosen = 2;
        }
	}
}
