﻿/*
name: Brieg Oudeacoumar
course: CST306
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Milestone 2 UPDATE :
 * - added FireTimer
 * - code clarification
*/

public class Placement : MonoBehaviour {

	public bool isFrozen = false;
	private bool fireSet = false;
	public GameObject currenrtHole;
	//public Rigidbody wood;
	public GameObject shovel;
	public GameObject lvl;
	private float nextDouse;
	private float timeInterval = 1f;
	void Start() {

	}

	// Update is called once per frame
	void Update() {

	}

	void OnCollisionEnter(Collision collision)
	{
		if (Time.time > nextDouse) {
			
			//If Log meets a FirePit
			if (collision.collider.tag == "dirt" && collision.collider.gameObject.GetComponent<firePit> ().alreadyUsed == false && isFrozen == false) {
				//wood.constraints = RigidbodyConstraints.FreezeAll;
				//Mark Log as part of a FirePit
				isFrozen = true;
				//Remember which FirePit the Log is part of
				currenrtHole = collision.collider.gameObject;
				Debug.Log ("Adding Log to FirePit (Through Dirt)");
				currenrtHole.GetComponent<firePit> ().allLogs.Add (this.gameObject);
				currenrtHole.GetComponent<firePit> ().fuel = true;
				currenrtHole.GetComponent<firePit> ().nbOfLogs++;
				Debug.Log ("Number of logs in FirePit : " + currenrtHole.GetComponent<firePit> ().nbOfLogs);

				//Update Scoreboard
				lvl.GetComponent<Manager> ().fi = lvl.GetComponent<Manager> ().fi + 1;
				Debug.Log ("Log added to Firepit");
			}
			//If Log meets another Log that is already part of a FirePit
			if (collision.collider.tag == "log" && collision.collider.gameObject.GetComponent<Placement> ().isFrozen == true && collision.collider.gameObject.GetComponent<Placement> ().currenrtHole.GetComponent<firePit> ().alreadyUsed == false && !isFrozen) {
				//wood.constraints = RigidbodyConstraints.FreezeAll;
				//Mark Log as part of a FirePit
				isFrozen = true;
				//Remember which FirePit the Log is part of
				currenrtHole = collision.gameObject.GetComponent<Placement> ().currenrtHole;
				Debug.Log ("Adding Log to FirePit (Through wood)");
				currenrtHole.GetComponent<firePit> ().allLogs.Add (this.gameObject);
				currenrtHole.GetComponent<firePit> ().nbOfLogs++;
				Debug.Log ("Number of logs in FirePit : " + currenrtHole.GetComponent<firePit> ().nbOfLogs++);

				//Update Scoreboard
				lvl.GetComponent<Manager> ().fi = lvl.GetComponent<Manager> ().fi + 1;
				Debug.Log ("Log added to Firepit");
			}
			//If Log meets Fire and Log is part of FirePit that is not lit
			if (collision.collider.tag == "fire" && !fireSet && isFrozen) {
				//Set FirePit on Fire
				Debug.Log ("Setting Fire to Firepit");
				currenrtHole.GetComponent<firePit> ().fire = true;
				fireSet = true;
				//Add to FireTimer 10 seconds of burn per Log in FirePit
				currenrtHole.GetComponent<firePit> ().fireTimer = (currenrtHole.GetComponent<firePit> ().nbOfLogs * 10);
				Debug.Log ("FirePit set to burn for : " + currenrtHole.GetComponent<firePit> ().fireTimer + " seconds");

				//Update Scoreboard
				lvl.GetComponent<Manager> ().wa = lvl.GetComponent<Manager> ().wa + 1;
				Debug.Log ("FirePit set on Fire");
				Debug.Log ("fireLevel set to : 3");
			}
			nextDouse = Time.time + timeInterval;
		}

	}
	private void OnCollisionExit(Collision collision)
	{
		//If Log is part of FirePit
		if (isFrozen == true)
		{
			//If Log doesn't touch FirePit or Log anymore
			if (collision.collider.tag != "dirt" && collision.collider.tag != "log"&& collision.collider.tag != "Rocks" && currenrtHole.GetComponent<firePit>().alreadyUsed == false)
			{
				Debug.Log("Log exiting Firepit");
				//Update nbOfLogs in FirePit
				currenrtHole.GetComponent<firePit>().nbOfLogs--;
				currenrtHole.GetComponent<firePit> ().allLogs.Remove(this.gameObject);
				//Mark Log as not part of a FirePit
				isFrozen = false;
				Debug.Log("Number of logs in FirePit : "+currenrtHole.GetComponent<firePit>().nbOfLogs);
				//Update Scoreboard
				lvl.GetComponent<Manager>().fi = lvl.GetComponent<Manager>().fi - 1;
			}

		}
	}
}
