﻿/*
name: Alejandro Ruvalcaba, Adrian Martinez
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class tpbackpack : MonoBehaviour
{
	public GameObject lvl;
	public GameObject player;
	public float floor;
	public Vector3 pos;
	private bool lvlSelected = true;
	private bool outsidePlayArea = false;
	void Start () 
	{
		
		pos = transform.position;

	}
	void Update () 
	{
	}
	void OnCollisionEnter(Collision collision)
	{
		if(!lvlSelected) {
			if (collision.collider.tag == "outofbound") 
			{
				transform.position = pos;
			}
			else if (collision.collider.tag == "NonCampsite")
			{
				if (outsidePlayArea) 
				{
					pos = player.transform.position;
					pos.y = 0.5f;
					transform.position = pos;
					outsidePlayArea = false;
				} 
				else 
				{
					pos = player.transform.position;
					pos.y = 0.5f;
				}

			}
			else if (collision.collider.tag == "level"){
				if (outsidePlayArea) {
					pos = player.transform.position;
					pos.y = 0.5f;
					transform.position = pos;
					outsidePlayArea = false;
				} 
				else {
					pos = player.transform.position;
					pos.y = 0.5f;
				}
			}
		}
		else{
			if (collision.collider.tag == "outofbound") {
					transform.position = pos;
			} 
			else if (collision.collider.tag == "NonCampsite"){
				transform.position = pos;
			}
			else if (collision.collider.tag == "level"){
				pos= transform.position;
			}
		}
	}
	void OnTriggerExit(Collider collision)
	{
		if (outsidePlayArea &&  !lvlSelected) {
			if (collision.tag == "Player") {
				outsidePlayArea = true;
	
			}
		}
	}
}
