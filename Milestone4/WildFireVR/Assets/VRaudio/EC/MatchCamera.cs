/*
name: Joshua Patrick, Alejandro Ruvalcaba
course: CST306 
*/
using UnityEngine;
using System.Collections;

public class MatchCamera : MonoBehaviour {
    static public int Scores = 0;
    static public int Level  = 1;

    public const int ScoresForNextLevel = 100;
    static public int Continuous = 0;

    #region Event Handler
    void Start() {
        NewGame();
    }

    
    void OnGUI() {
        

        GUIStyle textStyle = new GUIStyle();
        textStyle.normal.textColor  = Color.white;
        textStyle.fontSize = 36;

        
        GUILayout.BeginArea(new Rect(Screen.width / 2, Screen.height / 2, Screen.width/2, Screen.height / 2));
        
        GUILayout.Label("Scores:\t" + Scores, textStyle);
        GUILayout.Label("Level:\t" + Level,  textStyle);
        if (Continuous > 1)
            GUILayout.Label("Continuously:" + Continuous, textStyle);
        GUILayout.EndArea();
    }
    #endregion

    #region Helper function
    void NewGame() {
        Scores               = 0;
        Level                = 1;
        Continuous           = 0;

    }
    #endregion
}
