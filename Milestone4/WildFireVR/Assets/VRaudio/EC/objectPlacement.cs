﻿/*
name: Joshua Patrick, Adrian Martinez
course: CST306
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {
	private GameObject[] spawns = new GameObject[6];
	public GameObject[] types = new GameObject[2];
	public GameObject player;
	public GameObject start;
	// Use this for initialization
	void Start () {
		//Start Position
		int rand = Random.Range (0, 4);
		GameObject startPos = start.transform.GetChild (rand).gameObject;
		player.transform.position = startPos.transform.GetChild (0).transform.position;
		player.transform.rotation = startPos.transform.GetChild (0).transform.rotation;
		Vector3 fix = player.transform.rotation.eulerAngles;
		fix.y = fix.y - 90f;
		player.transform.rotation = Quaternion.Euler (fix);
		for (int i = 0; i < 3; i++) {
			GameObject clone;
			if (i < 1)
				clone = Instantiate(types[0]);
			else
				clone = Instantiate(types[1]);
			clone.SetActive(true);
			clone.transform.position = startPos.transform.GetChild (i+1).transform.position;
		}

		// 3 random
		for (int i = 0; i < 6; i++) {
			spawns [i] = transform.GetChild (0).transform.GetChild (i).gameObject;
		}
		int rand1 = Random.Range (0, 6);
		int rand2 = Random.Range (0, 6);
		int rand3 = Random.Range (0, 6);
		while (rand2 == rand1) {
			rand2 = Random.Range (0, 6);
		}
		while (rand3 == rand1 || rand3 == rand2) {
			rand3 = Random.Range (0, 6);
		}
		for (int i = 0; i < 3; i++) {
			GameObject clone;
			if (i < 2)
				clone = Instantiate(types[0]);
			else
				clone = Instantiate(types[1]);
			clone.SetActive(true);
			//clone.transform.rotation.eulerAngles = accuracy;
			if (i == 0)
				clone.transform.position = spawns[rand1].transform.position;
			else if (i == 1)
				clone.transform.position = spawns[rand2].transform.position;
			else
				clone.transform.position = spawns[rand3].transform.position;
		}

	}
	
	// Update is called once per frame
	/*
	void Update () {
		switch (gameState) {
		case GameState.Start:
			if (Input.GetMouseButtonUp (0)) {
				Shuffle ();
				gameState = GameState.Playing;
			}
			break;
		case GameState.Playing:
			CheckPieceInput ();
			break;
		case GameState.Animating:
			AnimateMovement (PieceToAnimate, Time.deltaTime);
			CheckIfAnimationEnded ();
			break;
		case GameState.End:
			if (Input.GetMouseButtonUp (0)) {   //reload
				Application.LoadLevel (Application.loadedLevel);
			}
			break;
		default:
			break;
		}
	}


	void OnGUI()
    {
        switch (gameState)
        {
            case GameState.Start:
                GUI.Label(new Rect(0, 0, 100, 100), "Tap to start!");
                break;
            case GameState.Playing:
                break;
            case GameState.End:
                GUI.Label(new Rect(0, 0, 100, 100), "Congrats, tap to start over!");
                break;
            default:
                break;
        }
    }
	*/

	private Vector3 GetScreenCoordinatesFromVieport(int i, int j)
    {
        
        Vector3 point = Camera.main.ViewportToWorldPoint(new Vector3(0.25f * j, 1 - 0.25f * i, 0));
        point.z = 0;
        return point;
    }
}

