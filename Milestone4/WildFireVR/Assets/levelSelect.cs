﻿/*
name: Alejandro Ruvalcaba
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelSelect : MonoBehaviour {
	public GameObject lvlManager;
    public GameObject lHand;
    public GameObject rHand;
    public bool canTeleport = false;
    private void OnCollisionEnter(Collision collision)
    {
		if (!canTeleport) {
			if (collision.collider.tag == "BadCampsite") {
				lvlManager.GetComponent<Manager> ().levelChosen = 1;
				lvlManager.GetComponent<Manager> ().badCampsite = true;
			}
			if (collision.collider.tag == "GoodCampsite") {
				lvlManager.GetComponent<Manager> ().levelChosen = 1;
				lvlManager.GetComponent<Manager> ().goodCampsite = true;
			}
			if (collision.collider.tag == "AverageCampsite") {
				lvlManager.GetComponent<Manager> ().levelChosen = 1;
				lvlManager.GetComponent<Manager> ().averageCampsite = true;
			}
		}
    }

}
