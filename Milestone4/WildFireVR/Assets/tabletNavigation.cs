﻿/*
name: Adrian Martinez
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class tabletNavigation : MonoBehaviour 
{
	public Text txt;
	public GameObject left;
	public GameObject middle;
	public GameObject right;
	public GameObject lvl;
	public bool leftHand = false;
	public bool rightHand = false;
	int x = 0;
	string[]tips=  {"Digging pits can be prohibited due to archaeological concerns," +
		" Don't build fires in dry conditions," +
		"Choose a site that is at least 15 feet away, from trees and tents," +
		"chose an open and level location," +
		"Choose a spot that is protected from wind," +
		"Pick a good site, " +
		"Always have water ready to put out fire," +
		"Dig a good sized hole for the fire," +
		"place rocks around fire," +
		"never leave fire unattended," +
		"make sure your fire does not get out of hand," +
		"Never place too much wood in fire"
	};
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(lvl.GetComponent<Manager>().levelChosen == 0)
		{
			txt.text = "To select campsite, place backpack on tent.";
		}
		else
		{
		if (leftHand)
		{
			left.GetComponent<Swipe> ().lhand = true;
			middle.GetComponent<Swipe> ().lhand = true;
			right.GetComponent<Swipe> ().lhand = true;


		}
		if (rightHand) 
		{
			left.GetComponent<Swipe> ().rhand = true;
			middle.GetComponent<Swipe> ().rhand = true;
			right.GetComponent<Swipe> ().rhand = true;
				x--;
		}
		if(!leftHand && !rightHand){
			left.GetComponent<Swipe> ().lhand = false;
			middle.GetComponent<Swipe> ().lhand = false;
			right.GetComponent<Swipe> ().lhand = false;
			left.GetComponent<Swipe> ().rhand = false;
			middle.GetComponent<Swipe> ().rhand = false;
			right.GetComponent<Swipe> ().rhand = false;
		}
		if(left.GetComponent<Swipe>().leftSwipe && middle.GetComponent<Swipe>().leftSwipe && right.GetComponent<Swipe>().leftSwipe){
			Debug.Log ("Left Swipe");
			left.GetComponent<Swipe> ().reset = true;
			middle.GetComponent<Swipe> ().reset = true;
			right.GetComponent<Swipe> ().reset = true;

				x++;
				if (x > 11) {
					x--;
				}
				txt.text = tips [x];
		}
		else if(left.GetComponent<Swipe>().rightSwipe && middle.GetComponent<Swipe>().rightSwipe && right.GetComponent<Swipe>().rightSwipe){
			Debug.Log ("Right Swipe");
			left.GetComponent<Swipe> ().reset = true;
			middle.GetComponent<Swipe> ().reset = true;
			right.GetComponent<Swipe> ().reset = true;

				x--;
				if (x < 0) {
					x++;
				}
				txt.text = tips [x];
		}

		else {
			//Do Nothing
		}
		}
	}
}
