﻿/*
name: Joshua Patrick, Alejandro Ruvalcaba
course: CST306
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeepScore : MonoBehaviour {
 
    public int logs;
    public int rocks;

    int score = 100;
	public float speed;
	public Text scoreCount;
	private Rigidbody rb;
	private int count = 100;

	// Use this for initialization
	void Start () {
        score = 100;		
	}
	
	// Update is called once per frame
	void Update () {
		if(logs < 6)
        {
            score -= (logs * 10);
        }
	}
}
