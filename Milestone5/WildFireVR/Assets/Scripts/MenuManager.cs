﻿/*
name: Adrian Martinez
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MenuManager : MonoBehaviour{

	void Start () {
		
	}
    public void LoadMyScene(int level) {
        SceneManager.LoadScene(level);
    }
    public void QuitScene() {
        Application.Quit();
        Debug.Log("Game is quitting!");
    }
	void Update ()
	{
	}
}
