﻿/*
name: Adrian Martinez
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Tablet : MonoBehaviour {
	public Material[] materials;
	public Renderer Rend; 
	private int index = 1;
	private SteamVR_TrackedObject trackedobj;
	private SteamVR_Controller.Device Controller{
		get{return SteamVR_Controller.Input((int)trackedobj.index);}
	}
	void Awake(){
		trackedobj = GetComponent<SteamVR_TrackedObject> ();
	}
	void Start (){
		Rend = GetComponent<Renderer> ();
		Rend.enabled = true;
	}
	void OnMouseDown(){
		if (materials.Length == 0) {
			return;
		}
		if (Input.GetMouseButtonDown (0)){
			index += 1;
			if (index == materials.Length + 1) {
				index = 1;
			}
			print (index);
			Rend.sharedMaterial = materials [index - 1]; 
		}
	}
	void OnTriggerEnter(Collider collision) {
		if (collision.tag == "lhand" || collision.tag == "rhand") {
			if (materials.Length == 0) {
				return;
			}
			if (Controller.GetHairTriggerDown()) {
				index += 1;
				if (index == materials.Length + 1){
					index = 1;
				}
				print (index);
				Rend.sharedMaterial = materials [index - 1]; 
		}
	}
}
}