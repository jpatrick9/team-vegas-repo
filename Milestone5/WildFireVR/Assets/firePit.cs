﻿/*
name: Brieg Oudeacoumar
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Milestone 2 UPDATE :
 * - added Log Counter
 * - added Red/Grey Ash mechanic
 * - added FireTimer
 * - fixed Particle systems
 * - fixed Fire Stages Transitions
 * - code clarification
*/

public class firePit : MonoBehaviour {



	private int cancoon = 0;
	public bool alreadyUsed = false;
	public bool fuel = false;
	public bool fire = false;
	public bool isRedAsh = false;
	public float fireTimer = 0;
	public int nbOfLogs = 0;
	public GameObject largeFire;
	public GameObject mediumFire;
	public GameObject smallFire;
	public GameObject smoke;
	public GameObject ashBlack;
	public GameObject ashRed;
	private GameObject lastFire;
	private GameObject lastAsh;
	public List<GameObject> allLogs;
	private int fireLvl = 0;
	private Vector3 pos;
	private Vector3 pos2;
	private Quaternion rot;
	private Quaternion rot2;
	private float nextDouse;
	private float timeInterval = 1f;
	public GameObject shovel;
	public ParticleSystem water;
	public GameObject lvlManager;
	private float uhuh = 0.0f;
	public List<ParticleCollisionEvent> collisionEvents;
	// Use this for initialization
	void Start()
	{
		//Initialization of variables
		rot = Quaternion.Euler(270, 0, 0);
		rot2 = Quaternion.Euler(-90, 0, 0);
		pos = transform.position;
		pos2 = new Vector3(transform.position.x, -0.04f, transform.position.z);
		water = GetComponent<ParticleSystem>();
		collisionEvents = new List<ParticleCollisionEvent>();
		allLogs = new List<GameObject> ();
		//All logs

	}
	// Update is called once per frame
	void Update()
	{
		//Reduce fireTimer by 1 second every second
		fireTimer -= Time.deltaTime;
		//If FirePit on Fire and fireTimer reaches 0
		if (fire && fireTimer < 0)
		{
			//Debug.Log("fireTimer reached 0");
			fireLvl = 0;
			fire = false;
			alreadyUsed = true;
			//Debug.Log("Turning Logs to RedAsh");

			Destroy (lastFire);
			isRedAsh = true;
			//Instantiate Smoke Particles
			lastFire = (GameObject)Instantiate(smoke, pos, rot);
			//Instantiate RedAsh Prefab
			lastAsh = (GameObject)Instantiate(ashRed, pos2, rot2);
			//Debug.Log("FirePit is now RedAsh");
		}
		//If FirePit is on Fire and Logs attached to FirePit and Firelvl > 0
		if (fire && fuel && fireLvl != -1)
		{
			foreach (GameObject Log in allLogs) {
				Log.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionY;
				Log.GetComponent<Rigidbody> ().isKinematic = true;
			}
			//Consume Logs
			fuel = false; 
			//Set fireLvl to maximum 
			fireLvl = 3;
			Destroy (lastFire);
			lastAsh = (GameObject)Instantiate(ashRed, pos2, rot2);
			lastFire = (GameObject)Instantiate(largeFire, pos, rot);
			//Disable FirePit digging
			shovel.GetComponent<digHole>().finish = true;
		}
	}
	//avner's pain
	void OnTriggerEnter(Collider other) {
		//Debug.Log (other.tag);
		if(fire)
		{
			if (other.tag == "lhand" || other.tag == "rhand") {
				if (Time.time > uhuh) {
					lvlManager.GetComponent<Manager> ().cancoon++;
					lvlManager.GetComponent<Manager> ().avBot = true;
					uhuh = Time.time + 3.0f;
				}
			}
		}
	}
	//Extinguish fire
	void OnTriggerExit(Collider collision)
	{
		//Debug.Log ("Triggered with" + collision.tag);
		if (Time.time > nextDouse)
		{
			//If FirePit is on Fire and meets water and is RedAsh
			if (collision.tag == "water" && isRedAsh)
			{
				//Debug.Log("RedAsh met Water");
				//Debug.Log("Turning RedAsh to BlackAsh");
				isRedAsh = false;
				//Destroy RedAsh
				Destroy(lastAsh);
				//Destroy Smoke
				//Instantiate BlackAsh
				lastAsh = (GameObject)Instantiate(ashBlack, pos2, rot2);
				//Debug.Log("FirePit off");
				//Update LevelManager
				lvlManager.GetComponent<Manager>().calculateScore = true;
			}
			//If FirePit is on Fire and meets water and fireLvl greater than 0
			if (fire && collision.tag == "water")
			{
				Destroy(lastFire);
				//Debug.Log("FirePit met water");
				//If FirePit level to max (3)
				if (fireLvl == 3)
				{
					//Instantiate smaller fire
					lastFire = (GameObject)Instantiate(mediumFire, pos, rot);
					fireLvl = fireLvl - 1;
					//Debug.Log("fireLvl set to : " + fireLvl);
				}
				else if (fireLvl == 2)
				{
					//Instantiate smaller fire
					lastFire = (GameObject)Instantiate(smallFire, pos, rot);
					fireLvl = fireLvl - 1;
					//Debug.Log("fireLvl set to : " + fireLvl);
				}
				else if (fireLvl == 1)
				{
					//Debug.Log("Turning FirePit to BlackAsh");
					alreadyUsed = true;
					isRedAsh = false;
					Destroy (lastAsh);
					//Instantiate smaller fire
					lastFire = (GameObject)Instantiate(smoke, pos, rot);
					//Instantiate RedAsh
					lastAsh = (GameObject)Instantiate(ashBlack, pos2, rot2);
					lastFire = null;
					fireLvl = -1;
					//Debug.Log("fireLvl set to : " + fireLvl);
					lvlManager.GetComponent<Manager>().calculateScore = true;
				}

			}
			nextDouse = Time.time + timeInterval;
		}
	}
}