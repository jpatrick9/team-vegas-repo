﻿/*
name: Adrian Martinez
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class tabletNavigation : MonoBehaviour 
{
	public Text txt;
	public GameObject lvl;
	int x = 0;
	public bool left = false;
	public bool right = false;
	private bool salt = false;
	string[]tips= new string[] {">Pick a camp site that doesn't have many trees", ">Don't build fires in dry conditions",">Make sure to put out your fire", ">Choose an open and level location", ">Make sure you dig a Med. size hole", ">Put rocks around your fire pit " ,">Always have water ready to put out fire",
		">Never leave fire unattended" ,">Make sure your fire doesn't get out of hand" ,">Never place too much wood in fire"
	};
	void Start () 
	{
	}
	void Update ()
	{
		if (lvl.GetComponent<Manager> ().levelChosen == 0 && !salt) {
			txt.text = "To select campsite, place backpack on tent. Grab arrows to swipe right or left for more hot tips!";
			salt = true;
		} 
		else {
			if (right) {
				Debug.Log ("Right Swipe");

				if (x > 9) {
					x--;
				} else {
					x++;
				}
				Debug.Log (x);
				txt.text = tips [x];
				right = false;
			} else if (left) {
				Debug.Log ("Left Swipe");

				if (x < 0) {
					x++;
				} else {
					x--;
				}
				Debug.Log (x);
				txt.text = tips [x];

				left = false;
			}
		}

	
	
}
}
