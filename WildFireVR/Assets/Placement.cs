﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Placement : MonoBehaviour {
	
	private bool isFrozen = false;
	private bool fireSet  = false;
	public GameObject currenrtHole;
	public Rigidbody wood;
	public GameObject shovel;
	public GameObject lvl;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.tag == "dirt") {
			wood.constraints = RigidbodyConstraints.FreezeAll;
			isFrozen = true;
			collision.collider.gameObject.GetComponent<firePit> ().fuel = true;
			currenrtHole = collision.collider.gameObject;
			shovel.GetComponent<digHole> ().finish = true;
			lvl.GetComponent<Manager> ().fi = lvl.GetComponent<Manager> ().fi + 1;
		}
		if (collision.collider.tag == "log" && collision.gameObject.GetComponent<Placement> ().isFrozen == true) {
			wood.constraints = RigidbodyConstraints.FreezeAll;
			isFrozen = true;
			currenrtHole = collision.gameObject.GetComponent<Placement> ().currenrtHole;
			lvl.GetComponent<Manager> ().fi = lvl.GetComponent<Manager> ().fi + 1;
		}
		if (collision.collider.tag == "fire" && !fireSet && isFrozen) {
			currenrtHole.GetComponent<firePit> ().fire=true;
			fireSet = true;
			lvl.GetComponent<Manager> ().wa = lvl.GetComponent<Manager> ().wa + 1;

		}
	}
}

